﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VerticalDrive
{
    public class InitiateMainMenu : MonoBehaviour
    {
        public void ShowTitle ()
        {
            Animator titleAnimator = GameObject.Find("MainMenu_Title").GetComponent<Animator>();

            titleAnimator.SetBool("Enabled", true);
        }

        public void ShowButtons ()
        {
            //Animator startButtonAnimator = GameObject.Find("MainMenu_Start_Button").GetComponent<Animator>();
            //Animator scoreboardButtonAnimator = GameObject.Find("MainMenu_Scoreboard_Button").GetComponent<Animator>();
            Animator copyrightAnimator = GameObject.Find("MainMenu_Copyright").GetComponent<Animator>();

            //startButtonAnimator.SetBool("Enabled", true);
            //scoreboardButtonAnimator.SetBool("Enabled", true);
            copyrightAnimator.SetBool("Enabled", true);
        }

        public void ShowPlay ()
        {
            Animator playBlackFadeAnimator = GameObject.Find("Play_Black").GetComponent<Animator>();

            playBlackFadeAnimator.ResetTrigger("FadeOut");
            playBlackFadeAnimator.SetTrigger("FadeOut");

            GameController.Instance._StartGame();
        }

        public void CallShowGameOver ()
        {
            GameController.Instance.ShowGameOver(true);
        }

        public void ResetGame ()
        {
            GameController.Instance.ResetGame();
        }
    }
}
