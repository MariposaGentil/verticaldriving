﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports; // PlayerPrefs > Api Compatibility Level > .NET 2.0
using System;

public class SerialCommunicator : MonoBehaviour
{
    //private Control Control;
    private SerialPort StreamTouchPanel;        

	private string currentInstruction = "";
	private string previousInstruction = "";

	private float lastSentTime;

    // isNotified is necessary to stop throwing Open Error several times
    private bool isNotified;

    private const float TIME_DELAY = 0.05f;


    private void Start ()
    {
        SetScripts();

        DetectPort();
		//StartCoroutine(DetectPort());
	}


	/*IEnumerator*/protected void DetectPort ()
    {
		//yield return new WaitUntil(() => Control.PortReady);
        //Debug.Log("SerialCommunication_AmbientLight.cs :: Port found: " + Control.Port);

        try
        {
            StreamTouchPanel = new SerialPort(/*Control.Port*/"COM5", 115200);

            StreamTouchPanel.Open();

            //_SendInstruction("a"); // Autonomous mode by default
            //_SendInstruction("c"); // Cold temperature by default
        }
        catch
        {
            //
        }

        Debug.Log("SerialCommunication_AmbientLight.cs :: Port opened: " + StreamTouchPanel.IsOpen);

        lastSentTime = Time.time;
	}

	private void Update ()
    {
		if (Time.time - lastSentTime >= TIME_DELAY && currentInstruction != previousInstruction)                
        {
            if (StreamTouchPanel != null)
            {
                if (StreamTouchPanel.IsOpen)
                {
                    try
                    {
                        //Debug.Log(StreamTouchPanel.ReadExisting());

                        //StreamTouchPanel.Write(currentInstruction);
                        //Debug.Log("SerialCommunication_AmbientLight.cs :: Written instruction: " + currentInstruction);

                        previousInstruction = currentInstruction;
                        lastSentTime = Time.time;
                    }
                    catch
                    {
                        //
                    }
                }
                else
                {
                    OpenPort(StreamTouchPanel);
                }
            }
		}
    }

	private void OnApplicationQuit ()
	{
		if (StreamTouchPanel.IsOpen)
        {
			StreamTouchPanel.Close();
		}
	}

	private void OpenPort (SerialPort port)
    {
		bool success = true;

		try
        {
			port.Open();
		}
		catch (Exception e)
        {
			success = false;

			if (!isNotified)
            {
				//Debug.LogError("Serial Port can not be open due to " + e.Message);
				isNotified = true;
			}
		}

		if (success)
        {
			isNotified = false;
		}

		if (port.IsOpen)
        {
			//port.Write("z");
		}
	}

    public void _SendInstruction (string instruction)
    {
        currentInstruction = instruction + "\n";
    }

	public void Reset ()
    {
		currentInstruction = "r\n";
	}


    private void SetScripts ()
    {
        //ScriptManager ScriptManager = GameObject.Find("Main").GetComponent<ScriptManager>();

        //Control = ScriptManager.Control;
    }

    /*
    if (StreamTouchPanel != null)                                
    {
		if (StreamTouchPanel.IsOpen)
        {
            try
            {
                if (Time.time - lastSentTime >= TIME_DELAY && currentInstruction != previousInstruction)
                {
                    StreamTouchPanel.Write(currentInstruction);
                    previousInstruction = currentInstruction;
                    lastSentTime = Time.time;

                    int BytesToRead = StreamTouchPanel.BytesToRead;
                    string empty = "";

                    if (BytesToRead > 0)
                    {
                        Debug.Log(BytesToRead);

                        while (BytesToRead > 0)
                        {
                            --BytesToRead;
                            empty += StreamTouchPanel.ReadChar();
                        }

                        Debug.Log(empty);
                    }
                }
            }
            catch
            {
                //
            }
                        
            }
        else
        {
            OpenPort(StreamTouchPanel);
        }				
	}
    */
}