﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class CarSpawner : MonoBehaviour
    {
        public static CarSpawner Instance { get; protected set; }

        protected Transform PlayTransform;

        public List<GameObject> disabledVehicle = new List<GameObject>();
        public List<GameObject> enabledVehicle = new List<GameObject>();

        protected int _currentTraffic;
        public int CurrentTraffic
        {
            get { return _currentTraffic; }
            set
            {
                _currentTraffic = value;
            }
        }

        protected const float SPAWN_POSITION = 1300f;

        protected float currentTime = 0;
        protected float timeBetweenSpawns = 2.5f;
        private float originalTimeBetweenSpawns;
        
        protected int maximunTraffic;
        protected int newLane;
        protected int storedLane;
        public float[] lanesProb; 


        protected void Awake ()
        {
            Instance = this;
            originalTimeBetweenSpawns = timeBetweenSpawns;
            lanesProb = new float[GameController.ROAD_LANES_NUM];
            for (int i = 0; i < GameController.ROAD_LANES_NUM; i++)
                lanesProb[i] = 1.0f / GameController.ROAD_LANES_NUM;
        }


        protected void Start ()
        {
            SetComponents();
            SetTrafficCars();
            SetDefaultValues();
        }


        public void SetDefaultValues ()
        {
            _currentTraffic = 0;
            maximunTraffic = 3; 
        }
        

        protected void Spawn ()
        {
            if (GameController.Instance.Playing)
            {
                bool broke = false; 
                for (int lane = 0; lane < GameController.ROAD_LANES_NUM && !broke; lane++)
                {
                    float rnd = Random.value;
                    if (rnd < lanesProb[lane])
                    {
                        newLane = lane;
                        broke = true;
                    }
                }

                lanesProb[newLane] /= (GameController.ROAD_LANES_NUM * 2);
                float total = 1 - lanesProb[newLane];

                for(int lane = 0; lane < GameController.ROAD_LANES_NUM; lane++)
                {
                    if (lane != newLane)
                        lanesProb[lane] = total / (GameController.ROAD_LANES_NUM - 1);
                }

               // while (newLane == storedLane)
               // {
               //     newLane = Random.Range(0, GameController.ROAD_LANES_NUM);
               // }

                storedLane = newLane;

                Vector3 position = new Vector3
                {
                    x = -GameController.ROAD_LANE_SEPARATION * (GameController.ROAD_LANES_NUM / 2 - newLane),
                    y = SPAWN_POSITION,
                    z = 0f
                };

                int newVehicle = Random.Range(0, disabledVehicle.Count);

                disabledVehicle[newVehicle].GetComponent<FoeCarController>().CurrentState = FoeCarController.State.Enabled;

                disabledVehicle[newVehicle].transform.localPosition = position;

                enabledVehicle.Add(disabledVehicle[newVehicle]);
                disabledVehicle.RemoveAt(newVehicle);                

                ++_currentTraffic;
            }
        }


        protected void Update ()
        {
            if (GameController.Instance.Playing)
            {
                currentTime += Time.deltaTime;

                if (currentTime >= timeBetweenSpawns && _currentTraffic < (Mathf.Min(Mathf.FloorToInt(maximunTraffic * (((GameController.Instance.DIFFICULTY_MULTIPLIER - 1) / 2) + 1)), 7)))
                {
                    currentTime = 0;
                    Spawn();
                }

                if (Time.frameCount % 25 == 0)
                {
                    foreach (GameObject car in enabledVehicle)
                    {
                        FoeCarController control = car.GetComponent<FoeCarController>();
                        control.Speed = control.originalSpeed * GameController.Instance.DIFFICULTY_MULTIPLIER;
                    }
                    timeBetweenSpawns = originalTimeBetweenSpawns / (GameController.Instance.DIFFICULTY_MULTIPLIER);
                }
            }
        }


        protected void SetTrafficCars ()
        {
            // Get all cars in a temp List
            Object[] trafficCarTemp = Resources.LoadAll("Prefabs/TrafficCar", typeof(GameObject));
            
            List<GameObject> disabledVehicleTemp = new List<GameObject>();

            for (int n = 0; n < 2; ++n) // There will be two copies of each car
            {
                for (int i = 0; i < trafficCarTemp.Length; ++i)
                {
                    disabledVehicleTemp.Add((GameObject)trafficCarTemp[i]);
                }
            }

            // Once they are all instantiated, we store them in a local list
            for (int r = 0; r < disabledVehicleTemp.Count; ++r)
            {
                GameObject vehicle = Instantiate(disabledVehicleTemp[r].gameObject, Vector3.zero, Quaternion.identity, PlayTransform);
                vehicle.transform.localPosition = new Vector3(9999f, 9999f, 0);

                vehicle.GetComponent<FoeCarController>().CurrentState = FoeCarController.State.Disabled;

                disabledVehicle.Add(vehicle);
            }

            SetFadeIndex();
        }

        // Set Index after all cars have been instantiated
        protected void SetFadeIndex ()
        {
            Transform playerCar = GameObject.Find("PlayerDragAnimation").transform;
            Transform score = GameObject.Find("Play_Score").transform;
            Transform playBlackFade = GameObject.Find("Play_Black").transform;
            Transform playWhiteFade = GameObject.Find("Play_White").transform;

            playerCar.SetSiblingIndex(PlayTransform.childCount);
            score.SetSiblingIndex(PlayTransform.childCount);
            playBlackFade.SetSiblingIndex(PlayTransform.childCount);
            playWhiteFade.SetSiblingIndex(PlayTransform.childCount);
        }

        protected void SetComponents ()
        {
            PlayTransform = GameObject.Find("Play").transform;
        }
    }
}
