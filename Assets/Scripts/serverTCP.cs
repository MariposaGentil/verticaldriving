﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine;
using System.Threading;

public class serverTCP{
	
	// Use this for initialization
	TcpListener server; 
	NetworkStream stream; 
	TcpClient client; 

	string connectKey = "846816_34sdDwer5663561"; 
 	static ManualResetEvent tcpClientConnected = new ManualResetEvent(false);
	long timeOut; 

	protected string ip;
	protected int port; 
	protected int timeOutLimit; 
	protected bool checkConection; 
	protected bool end;
	public serverTCP (int nport = 6050, int ntimeOutLimit = 1500, bool ncheckConection = false) {

		port = nport; 
		timeOutLimit = ntimeOutLimit;

		server = new TcpListener(IPAddress.Any, port);
		//server.Stop ();
		server.Start ();

		checkConection = ncheckConection; 
		end = false; 
	//	client = server.BeginAcceptTcpClient ();
	//	stream = client.GetStream(); 
	}
		
	~serverTCP()
	{
		server.Stop ();
		client.Close ();
		server = null;
		client = null; 
	}

	long timeLastRead; 

	public IEnumerator startReceiving()
	{
		bool timedOut = false;
		while (!end) {
			if (client == null) {
				timedOut = false; 
				DoBeginAcceptTcpClient (server);
				while (client == null)
					yield return null;
				timeOut = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
				Debug.Log ("Client connected");
			} else { 
				
				while (client.Available <= 0 && !timedOut) {
					if (timeOutLimit > 0) {
						if (System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - timeOut > timeOutLimit)
							timedOut = true;
						else
							timedOut = false; 
				//		control.consolePrintln ("Counting");
					}
					//Thread.Sleep (500);
					yield return null;
				}
				
				if (!timedOut) {

					Byte[] bytes = new Byte[client.Available];
					stream.Read (bytes, 0, bytes.Length);
					string received = Encoding.ASCII.GetString (bytes, 0, bytes.Length); 

					try {
				
						dataReceived (received); 

					} catch (Exception e)
                    {
					}


					timeOut = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

				} else {
					sendGoodBye ();
					client.Close (); 
					client = null; 
				}
				yield return null; 
			}
		}
	}

	// // // // // //
	protected virtual void dataReceived(string msg)
	{
        switch(msg)
        {
            case ("a"):
            {
                    VerticalDrive.GameController.Instance._MovePlayerLeft();
            }break;
            case ("d"):
            {
                VerticalDrive.GameController.Instance._MovePlayerRight();
            }break;
            default:
            {}break; 

        }
    }



	private void DoBeginAcceptTcpClient(TcpListener listener)
	{
		tcpClientConnected.Reset();
		Debug.Log("Waiting for a connection at: " + port);
		listener.BeginAcceptTcpClient(
			new AsyncCallback(DoAcceptTcpClientCallback), 
			listener);
		
		//tcpClientConnected.WaitOne();
	}

	// Process the client connection.
	private void DoAcceptTcpClientCallback(IAsyncResult ar) 
	{
		TcpListener listener = (TcpListener) ar.AsyncState;

		client = listener.EndAcceptTcpClient(ar);
		stream = client.GetStream ();
		client.GetStream ().Write (Encoding.UTF8.GetBytes("Hello"), 0, 1024);
		tcpClientConnected.Set();

		timeOut = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

		sendHello ();
	}

	protected void sendHello()
	{
		if(checkConection)
			if (client != null) 
			{
				byte[] bytes = Encoding.ASCII.GetBytes(connectKey + "Hello");
				client.GetStream ().Write (bytes, 0, bytes.Length);
			}
	}

	public void restart()
	{
		client.Close ();
		client = null;	
	}

	public void disconnect()
	{
		sendGoodBye ();
		end = true; 

		if (client != null) {
			stream.Close (); 
			stream = null; 
			client.Close ();
			client = null;
		}
		server.Stop ();
		server = null; 

	}
	protected void sendGoodBye()
	{
		if (checkConection) {
			try {
				if (client != null) {
					Debug.Log ("Client disconnected"); 
					byte[] bytes = Encoding.ASCII.GetBytes ("Goodbye" + connectKey);
					client.GetStream ().Write (bytes, 0, bytes.Length);
				}
			} catch (Exception e) {}
		}
	}
}
