﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class InputController : MonoBehaviour
    {
        protected bool controlsEnabled; 
        protected void KeyboardControls ()
        {
            // Start game
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (GameController.Instance.CurrentMenu == GameController.Menu.MainMenu)
                {
                    GameController.Instance._BeforeStart();
                }                
                else if (GameController.Instance.CurrentMenu == GameController.Menu.GameOver)
                {
                    GameController.Instance._CheckName();
                }
                else if (GameController.Instance.CurrentMenu == GameController.Menu.Scoreboard)
                {
                    GameController.Instance._FinishGame();
                }
            }

            // Move Player Car Left
            if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) && controlsEnabled)
            {
                GameController.Instance._MovePlayerLeft();
            }

            // Move Player Car Right
            if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) && controlsEnabled)
            {
                GameController.Instance._MovePlayerRight();
            }

            // Exit game and close application
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameController.Instance.ExitGame();
            }



            if (Input.GetKeyUp(KeyCode.F1))
            {
                controlsEnabled = !controlsEnabled;
            }

            // Reset entire game
            if (Input.GetKeyUp(KeyCode.F2))
            {
                GameController.Instance.ResetGame();
            }

            if(Input.GetKeyUp(KeyCode.F3))
            {
                Cursor.visible = !Cursor.visible;
            }
        }

        protected void Awake()
        {
            controlsEnabled = false;
            Cursor.visible = false; 
        }

        protected void Update ()
        {
            KeyboardControls();
        }
    }
}