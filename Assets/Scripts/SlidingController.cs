﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Linq;
using System;

namespace VerticalDrive
{
    public class SlidingController : MonoBehaviour
    {
        public static SlidingController Instance { get; protected set; }
        private SerialPort serial1;
        private byte[] buffer;
        private const int bufferCapacity = 256;

        void Awake()
        {
            Instance = this;
            buffer = new byte[bufferCapacity];
            OpenConnection();
        }

        void Update()
        {
            try
            {
                if (serial1.IsOpen)
                {
                    int char_read = serial1.Read(buffer, 0, bufferCapacity);

                    if (char_read > 0 && buffer[0] != '\0')
                    {
                        //Debug.Log(System.Text.Encoding.UTF8.GetString(buffer));

                        if (buffer[0] == 'a')
                        {
                            GameController.Instance._MovePlayerLeft();
                        }
                        else if (buffer[0] == 'd')
                        {
                            GameController.Instance._MovePlayerRight();
                        }
                    }
                    serial1.DiscardInBuffer();
                }
                else
                {
                    //Debug.Log("SlidingController :: Port CLOSED!");
                    CloseConnection();
                    OpenConnection();
                }
            }
            catch (Exception)
            {
                
            }
        }

        public void OpenConnection()
        {
            try
            {
                serial1 = new SerialPort("COM5", 115200);
                serial1.ReadTimeout = 1;
                serial1.Open();

                Debug.Log("SlidingController :: Port opened!");

                if (serial1.IsOpen)
                {
                    serial1.DiscardInBuffer();
                }
            }
            catch (Exception)
            {
                //Debug.Log("SlidingController :: Port NOT opened!");
            }
        }

        public void CloseConnection()
        {
            serial1.Dispose();
            serial1.Close();
        }
    }
}
