﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class InfiniteRoadController : MonoBehaviour
    {
        protected Animator animator;

        protected void Awake()
        {
            animator = GetComponent<Animator>();     
        }

        protected void Start ()
        {

        }


        protected void Update ()
        {
            animator.speed = GameController.Instance.DIFFICULTY_MULTIPLIER;
        }

    }
}