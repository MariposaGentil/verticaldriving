﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class PlayerController : MonoBehaviour
    {
        private void OnTriggerEnter2D (Collider2D other)
        {
            if (other.gameObject.tag == "Vehicle")
            {
                GameController.Instance.StopGame();
            }
        }
    }
}

