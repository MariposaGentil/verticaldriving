﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

namespace VerticalDrive
{
    public class DataController : MonoBehaviour
    {
        public static DataController Instance { get; protected set; }
        public ReadData ReadData;

        protected string _top1Name;
        protected string _top1Score;

        protected string _top2Name;
        protected string _top2Score;

        protected string _top3Name;
        protected string _top3Score;

        protected string _top4Name;
        protected string _top4Score;

        protected string _top5Name;
        protected string _top5Score;

        protected string _top6Name;
        protected string _top6Score;

        protected string _top7Name;
        protected string _top7Score;

        protected string _top8Name;
        protected string _top8Score;

        protected string _top9Name;
        protected string _top9Score;

        protected string _top10Name;
        protected string _top10Score;

        protected string[] TopName;
        protected string[] TopScore;

        protected string[] TopNameStored;
        protected string[] TopScoreStored;

        protected Text[] topNameText;
        protected Text[] topScoreText;


        protected void Awake ()
        {
            Instance = this;
        }


        protected void Start ()
        {
            SetArrays();
            SetComponents();

            LoadFile();
        }


        // Load ReadData values from json
        protected void LoadFile ()
        {
            ReadData = new ReadData();

            string readPath = Application.streamingAssetsPath + "/Data/data.json";
            using (StreamReader r = new StreamReader(readPath))
            {
                string jsonRead = r.ReadToEnd();
                JsonUtility.FromJsonOverwrite(jsonRead, ReadData);
            }

            GetData();
        }

        // Write json file with ReadData values
        protected void WriteFile ()
        {
            string savePath = Application.streamingAssetsPath + "/Data/data.json";

            File.WriteAllText(savePath, JsonUtility.ToJson(ReadData));
        }

        // Replace DataController's values with ReadData's
        protected void GetData ()
        {
            _top1Name = ReadData._top1Name;
            _top2Name = ReadData._top2Name;
            _top3Name = ReadData._top3Name;
            _top4Name = ReadData._top4Name;
            _top5Name = ReadData._top5Name;
            _top6Name = ReadData._top6Name;
            _top7Name = ReadData._top7Name;
            _top8Name = ReadData._top8Name;
            _top9Name = ReadData._top9Name;
            _top10Name = ReadData._top10Name;

            _top1Score = ReadData._top1Score;
            _top2Score = ReadData._top2Score;
            _top3Score = ReadData._top3Score;
            _top4Score = ReadData._top4Score;
            _top5Score = ReadData._top5Score;
            _top6Score = ReadData._top6Score;
            _top7Score = ReadData._top7Score;
            _top8Score = ReadData._top8Score;
            _top9Score = ReadData._top9Score;
            _top10Score = ReadData._top10Score;

            RefreshScoreboard();
        }

        // Replace ReadData's values with the new ones
        protected void SendData ()
        {
            ReadData._top1Name = TopName[0];
            ReadData._top2Name = TopName[1];
            ReadData._top3Name = TopName[2];
            ReadData._top4Name = TopName[3];
            ReadData._top5Name = TopName[4];
            ReadData._top6Name = TopName[5];
            ReadData._top7Name = TopName[6];
            ReadData._top8Name = TopName[7];
            ReadData._top9Name = TopName[8];
            ReadData._top10Name = TopName[9];

            ReadData._top1Score = TopScore[0];
            ReadData._top2Score = TopScore[1];
            ReadData._top3Score = TopScore[2];
            ReadData._top4Score = TopScore[3];
            ReadData._top5Score = TopScore[4];
            ReadData._top6Score = TopScore[5];
            ReadData._top7Score = TopScore[6];
            ReadData._top8Score = TopScore[7];
            ReadData._top9Score = TopScore[8];
            ReadData._top10Score = TopScore[9];

            WriteFile();
        }

        // Check if current player's score is better than any other in the savefile
        public void CheckNewTop (string name, int score)
        {
            int position = 20;

            if (score >= Convert.ToInt32(_top1Score))
            {
                position = 0;
            }
            else if (score >= Convert.ToInt32(_top2Score))
            {
                position = 1;
            }
            else if (score >= Convert.ToInt32(_top3Score))
            {
                position = 2;
            }
            else if (score >= Convert.ToInt32(_top4Score))
            {
                position = 3;
            }
            else if (score >= Convert.ToInt32(_top5Score))
            {
                position = 4;
            }
            else if (score >= Convert.ToInt32(_top6Score))
            {
                position = 5;
            }
            else if (score >= Convert.ToInt32(_top7Score))
            {
                position = 6;
            }
            else if (score >= Convert.ToInt32(_top8Score))
            {
                position = 7;
            }
            else if (score >= Convert.ToInt32(_top9Score))
            {
                position = 8;
            }
            else if (score >= Convert.ToInt32(_top10Score))
            {
                position = 9;
            }

            if (position < 15)
            {
                SetNewTop(position, name, score.ToString());
            }
        }

        // Write new player's score in data
        protected void SetNewTop (int position, string name, string score)
        {
            // Set every scoreboard position from current player's on
            for (int i = position + 1; i < 10; ++i)
            {
                TopName[i] = TopNameStored[i - 1];
                TopScore[i] = TopScoreStored[i - 1];
            }

            // Set current player's score
            TopName[position] = name;
            TopScore[position] = score;
            
            for (int i = 0; i < 10; ++i)
            {
                // Write table content
                topNameText[i].text = TopName[i];
                topScoreText[i].text = TopScore[i];

                // Refresh stored values
                TopNameStored[i] = TopName[i];
                TopScoreStored[i] = TopScore[i];
            }

            SendData();
        }

        // Refresh TopName and TopScore content after getting data from savefile
        protected void RefreshScoreboard ()
        {
            TopName = new string[10]
            {
                _top1Name,
                _top2Name,
                _top3Name,
                _top4Name,
                _top5Name,
                _top6Name,
                _top7Name,
                _top8Name,
                _top9Name,
                _top10Name
            };

            TopScore = new string[10]
            {
                _top1Score,
                _top2Score,
                _top3Score,
                _top4Score,
                _top5Score,
                _top6Score,
                _top7Score,
                _top8Score,
                _top9Score,
                _top10Score
            };

            for (int i = 0; i < 10; ++i)
            {
                // Write table content
                topNameText[i].text = TopName[i];
                topScoreText[i].text = TopScore[i];

                // Refresh stored values
                TopNameStored[i] = TopName[i];
                TopScoreStored[i] = TopScore[i];
            }
        }

        protected void SetArrays ()
        {
            TopName = new string[10];
            TopNameStored = new string[10];

            TopScore = new string[10];
            TopScoreStored = new string[10];
        }


        protected void SetComponents ()
        {
            Transform row01 = GameObject.Find("Row_01").transform;
            Transform row02 = GameObject.Find("Row_02").transform;
            Transform row03 = GameObject.Find("Row_03").transform;
            Transform row04 = GameObject.Find("Row_04").transform;
            Transform row05 = GameObject.Find("Row_05").transform;
            Transform row06 = GameObject.Find("Row_06").transform;
            Transform row07 = GameObject.Find("Row_07").transform;
            Transform row08 = GameObject.Find("Row_08").transform;
            Transform row09 = GameObject.Find("Row_09").transform;
            Transform row10 = GameObject.Find("Row_10").transform;

            topNameText = new Text[10]
            {
                row01.GetChild(0).GetComponent<Text>(),
                row02.GetChild(0).GetComponent<Text>(),
                row03.GetChild(0).GetComponent<Text>(),
                row04.GetChild(0).GetComponent<Text>(),
                row05.GetChild(0).GetComponent<Text>(),
                row06.GetChild(0).GetComponent<Text>(),
                row07.GetChild(0).GetComponent<Text>(),
                row08.GetChild(0).GetComponent<Text>(),
                row09.GetChild(0).GetComponent<Text>(),
                row10.GetChild(0).GetComponent<Text>()
            };

            topScoreText = new Text[10]
            {
                row01.GetChild(1).GetComponent<Text>(),
                row02.GetChild(1).GetComponent<Text>(),
                row03.GetChild(1).GetComponent<Text>(),
                row04.GetChild(1).GetComponent<Text>(),
                row05.GetChild(1).GetComponent<Text>(),
                row06.GetChild(1).GetComponent<Text>(),
                row07.GetChild(1).GetComponent<Text>(),
                row08.GetChild(1).GetComponent<Text>(),
                row09.GetChild(1).GetComponent<Text>(),
                row10.GetChild(1).GetComponent<Text>()
            };
        }
    }

    [System.Serializable]
    public class ReadData
    {
        public string _top1Name;
        public string _top1Score;

        public string _top2Name;
        public string _top2Score;

        public string _top3Name;
        public string _top3Score;

        public string _top4Name;
        public string _top4Score;

        public string _top5Name;
        public string _top5Score;

        public string _top6Name;
        public string _top6Score;

        public string _top7Name;
        public string _top7Score;

        public string _top8Name;
        public string _top8Score;

        public string _top9Name;
        public string _top9Score;

        public string _top10Name;
        public string _top10Score;
    }
}
