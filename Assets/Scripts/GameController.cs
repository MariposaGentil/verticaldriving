﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace VerticalDrive
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance { get; protected set; }

        public float DIFFICULTY_MULTIPLIER;
        public float DIFFICULTY_TO_ADD = 0.05f; 
        protected float timePlaying { get; private set; }

        protected CanvasGroup mainMenuCanvasGroup;
        protected CanvasGroup playCanvasGroup;
        protected Animator gameOverAnimator;
        protected Animator scoreboardAnimator;

        protected Transform playerTransform;
        protected Vector3 targetPosition;

        protected Animation playerDragAnimator;
        protected Animator roadAnimator;

        protected Animator pressStartAnimator;
        protected Animator menuBlackFadeAnimator;

        protected Animator playWhiteFadeAnimator;

        protected Text scoreText;
        protected InputField inputName;

        protected serverTCP gestureServer;
        protected IEnumerator gestureServerReceive;

        public enum Menu { MainMenu, Play, GameOver, Scoreboard };
        public Menu CurrentMenu { get; protected set; }

        // Game is playing
        public bool Playing { get; protected set; }
        // Enables or disables swapping road lane
        public bool MovementEnabled { get; protected set; }

        // Number of lane in wich the player is positioned. Starting from the left, lanes are: 0, 1, 2
        public int CurrentRoadLane { get; private set; }

        public const int ROAD_LANES_NUM = 3;

        public const float ROAD_LANE_SEPARATION = 295f;
        protected const float PLAYER_LANE_CHANGING_SPEED = 10f;
        protected const float MIN_DISTANCE = 0.05f;

        protected const float SCORE_MULTIPLIER = 27f;

        protected float _currentScore;
        public float CurrentScore
        {
            get { return _currentScore; }
            set
            {
                _currentScore = value;
                scoreText.text = Mathf.FloorToInt(_currentScore).ToString();
            }
        }

        protected float _temp;

        protected EventSystem m_EventSystem;

        protected void Awake ()
        {
            Instance = this;
            gestureServer = new serverTCP(6000, 50, false);
            gestureServerReceive = gestureServer.startReceiving();
            StartCoroutine(gestureServerReceive);
        }


        protected void Start ()
        {
            SetComponents();
            DIFFICULTY_MULTIPLIER = 1.0f; 

            Font newFont = (Font)Resources.Load("Fonts/bignoodletoo", typeof(Font));
            newFont.material.mainTexture.filterMode = FilterMode.Point;
            newFont.material.mainTexture.anisoLevel = 0;

            CurrentMenu = Menu.MainMenu;

            //ShowScoreboard(true);
        }


        public void _BeforeStart ()
        {
            pressStartAnimator.ResetTrigger("Started");
            pressStartAnimator.SetTrigger("Started");

            menuBlackFadeAnimator.ResetTrigger("FadeIn");
            menuBlackFadeAnimator.SetTrigger("FadeIn");

            //_StartGame();
        }

        public void _StartGame ()
        {
            SetDefaultValues();
            CarSpawner.Instance.SetDefaultValues();

            ShowMainMenu(false);
            ShowGameOver(false);
            ShowScoreboard(false);

            Playing = true;          
        }

        protected void SetDefaultValues ()
        {
            CurrentMenu = Menu.Play;

            playerTransform.localPosition = new Vector3(0, 0, 0);

            CurrentRoadLane = Mathf.FloorToInt(ROAD_LANES_NUM / 2); // Position the player on the center lane
        }

        protected void ShowMainMenu (bool state)
        {
            mainMenuCanvasGroup.alpha = state ? 1 : 0;
            mainMenuCanvasGroup.interactable = state;
            mainMenuCanvasGroup.blocksRaycasts = state;
        }

        protected void ShowPlay (bool state)
        {
            playCanvasGroup.alpha = state ? 1 : 0;
            playCanvasGroup.interactable = state;
            playCanvasGroup.blocksRaycasts = state;
        }

        public void ShowGameOver (bool state)
        {
            gameOverAnimator.SetBool("Enabled", state);

            if (state)
            {
                //EventSystem.current.SetSelectedGameObject(inputName.gameObject, null);
                //inputName.OnPointerClick(null);

                //inputName.Select();
                //inputName.Select
                //EventSystem.current.SetSelectedGameObject(inputName.gameObject, null);
                m_EventSystem.SetSelectedGameObject(inputName.gameObject);

                CurrentMenu = Menu.GameOver;
            }
            else
            {
                //GameObject.Find("GameOver_InputName").GetComponent<InputField>().DeactivateInputField();
            }            
        }

        void OnLoadScene ()
        {

        }

        public void _CheckName ()
        {
            if (GameObject.Find("GameOver_InputName").GetComponent<InputField>().text != "")
            {
                DataController.Instance.CheckNewTop(GameObject.Find("GameOver_InputName").GetComponent<InputField>().text, Mathf.FloorToInt(_currentScore));

                // SaveName
                ShowScoreboard(true);               
            }
        }

        public void ShowScoreboard (bool state)
        {
            scoreboardAnimator.SetBool("Enabled", state);

            if (state)
            {
                CurrentMenu = Menu.Scoreboard;
            }
        }

        public void _FinishGame ()
        {
            ShowMainMenu(false);
            ShowPlay(false);
            ShowGameOver(false);
            ShowScoreboard(false);
            //ShowScoreboard(false);
        }

        public void StopGame ()
        {
            EndGame();
        }

        protected void EndGame ()
        {
            Playing = false;
            MovementEnabled = false;

            foreach (AnimationState state in playerDragAnimator)
            {
                state.speed = 0;
            }

            DIFFICULTY_MULTIPLIER = 0;

            roadAnimator.speed = 0;

            for (int i = 0; i < CarSpawner.Instance.enabledVehicle.Count; ++i)
            {
                CarSpawner.Instance.enabledVehicle[i].GetComponent<FoeCarController>().Speed = 0;
            }

            playWhiteFadeAnimator.ResetTrigger("Crash");
            playWhiteFadeAnimator.SetTrigger("Crash");

            GameObject.Find("GameOver_Score").GetComponent<Text>().text = Mathf.FloorToInt(_currentScore).ToString();
        }

        public void ExitGame ()
        {
            Application.Quit();
        }

        public void _MovePlayerLeft () // @from InputController
        {
            if (Playing)
            {
                if (CurrentRoadLane != 0)
                {
                    playerDragAnimator.Stop();
                    playerDragAnimator.Play("PlayerLeft");
                }

                --CurrentRoadLane;

                if (CurrentRoadLane < 0)
                {
                    CurrentRoadLane = 0;
                }

                CalculateTargetPosition();

                MovementEnabled = true;
            }
        }

        public void _MovePlayerRight () // @from InputController
        {
            if (Playing)
            {
                if (CurrentRoadLane != 2)
                {
                    playerDragAnimator.Stop();
                    playerDragAnimator.Play("PlayerRight");
                }

                ++CurrentRoadLane;

                if (CurrentRoadLane >= ROAD_LANES_NUM)
                {
                    CurrentRoadLane = ROAD_LANES_NUM - 1;
                }

                CalculateTargetPosition();

                MovementEnabled = true;
            }            
        }

        protected void CalculateTargetPosition ()
        {
            targetPosition = playerTransform.localPosition;
            targetPosition.x = (CurrentRoadLane * ROAD_LANE_SEPARATION) - (ROAD_LANE_SEPARATION * Mathf.Floor(ROAD_LANES_NUM / 2));
            targetPosition.y = -500;
        }        

        protected void SwapRoadLane ()
        {
            if (MovementEnabled)
            {
                playerDragAnimator.transform.localPosition = Vector3.Lerp(playerDragAnimator.transform.localPosition, targetPosition, PLAYER_LANE_CHANGING_SPEED * Time.deltaTime);

                if (Vector3.Distance(playerDragAnimator.transform.localPosition, targetPosition) <= MIN_DISTANCE)
                {
                    playerDragAnimator.transform.localPosition = targetPosition;
                    MovementEnabled = false;
                }
            }
        }

        protected void AddScore ()
        {
            if (Playing)
            {
                CurrentScore += Time.deltaTime * SCORE_MULTIPLIER;
            }
        }

        protected int getSecsPlaying()
        {
            return Mathf.RoundToInt(CurrentScore / SCORE_MULTIPLIER);
        }

        protected void calculateDifficulty ()
        {
            if (timePlaying != getSecsPlaying())
            {
                timePlaying = getSecsPlaying();
                DIFFICULTY_MULTIPLIER += DIFFICULTY_TO_ADD;
            }

        }

        public void ResetGame ()
        {
            resetServers();
            SlidingController.Instance.CloseConnection();
            SceneManager.LoadScene(0);
        }


        protected void Update ()
        {
            SwapRoadLane();
            AddScore();
            calculateDifficulty();

            Input.GetKeyDown(KeyCode.Return);

            

            //inputName.Select();

            // Debug.Log(m_EventSystem + " " + inputName.isFocused);
        }


        protected void SetComponents ()
        {
            playerTransform = GameObject.Find("Player").transform;

            playerDragAnimator = GameObject.Find("PlayerDragAnimation").GetComponent<Animation>();
            roadAnimator = GameObject.Find("InfiniteRoad").GetComponent<Animator>();

            pressStartAnimator = GameObject.Find("MainMenu_PressStart").GetComponent<Animator>();
            menuBlackFadeAnimator = GameObject.Find("MainMenu_Black").GetComponent<Animator>();

            playWhiteFadeAnimator = GameObject.Find("Play_White").GetComponent<Animator>();

            mainMenuCanvasGroup = GameObject.Find("MainMenu").GetComponent<CanvasGroup>();
            playCanvasGroup = GameObject.Find("Play").GetComponent<CanvasGroup>();
            gameOverAnimator = GameObject.Find("GameOver").GetComponent<Animator>();
            scoreboardAnimator = GameObject.Find("Scoreboard").GetComponent<Animator>();

            scoreText = GameObject.Find("Play_Score").GetComponent<Text>();
            inputName = GameObject.Find("GameOver_InputName").GetComponent<InputField>();


            m_EventSystem = EventSystem.current;
        }

        public void resetServers()
        {
            StopCoroutine(gestureServerReceive);
            gestureServer.disconnect();
            gestureServerReceive = null; 
        }
    }
}
