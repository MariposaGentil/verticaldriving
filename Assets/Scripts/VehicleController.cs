﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class VehicleController : MonoBehaviour
    {
        [SerializeField]
        protected float speed = 0;

        private const float DESPAWN_POSITION = -10f;

        void Start ()
        {

        }

        void Update ()
        {
            if (GameController.Instance.Playing)
            {
                Vector3 pos = this.gameObject.transform.position;
                pos.y -= speed * Time.deltaTime;
                this.gameObject.transform.position = pos;

                if (this.transform.position.y <= DESPAWN_POSITION)
                {
                    Destroy(this.gameObject);
                }

                Collider2D collider = Physics2D.OverlapCircle(this.gameObject.transform.position + new Vector3(0, 1, 0), 1f);

                if (collider != null)
                {
                    if (collider.gameObject.tag == "Vehicle")
                    {
                        this.speed = collider.gameObject.GetComponent<VehicleController>().speed;
                    }
                }
            }
        }
    }
}