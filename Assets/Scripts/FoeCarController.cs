﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VerticalDrive
{
    public class FoeCarController : MonoBehaviour
    {
        protected float _speed = 8.5f;

        public float originalSpeed;
        private Animator animator;
        private AnimationEvent evt;

        public float Speed
        {
            get { return _speed; }
            set
            {
                _speed = value;
            }
        }

        protected void Awake()
        {
            originalSpeed = _speed;
            animator = GetComponent<Animator>();

            configureAnimation();
        }

        protected void configureAnimation()
        {
            evt = new AnimationEvent();
            evt.time = 39.5f;
            evt.functionName = "disableCar";

            AnimationClip clip = animator.runtimeAnimatorController.animationClips[0];
            clip.AddEvent(evt);
        }

        protected int _destroyYCoordinate = -1400;
        public int DestroyYCoordinate
        {
            get { return _destroyYCoordinate; }
            set
            {
                _destroyYCoordinate = value;
            }
        }

        public enum State { Enabled, Disabled };

        protected State _currentState;
        public State CurrentState
        {
            get { return _currentState; }
            set
            {
                _currentState = value;
                if (_currentState == State.Enabled) animator.SetBool("Enabled", true);
                else animator.SetBool("Enabled", false);
            }
        }

        
        public void disableCar()
        {
            CarSpawner.Instance.CurrentTraffic -= 1;

            transform.localPosition = new Vector3(9999f, 9999f, 0);

            for (int i = 0; i < CarSpawner.Instance.enabledVehicle.Count; ++i)
            {
                if (CarSpawner.Instance.enabledVehicle[i] == gameObject)
                {
                    CarSpawner.Instance.disabledVehicle.Add(CarSpawner.Instance.enabledVehicle[i]);
                    CarSpawner.Instance.enabledVehicle.RemoveAt(i);
                    CurrentState = State.Disabled; 
                }
            }
        }

        protected void Update ()
        {
            animator.speed = GameController.Instance.DIFFICULTY_MULTIPLIER * originalSpeed;
        }
        
    }
}

