This is the README for the vertical driving game of Tech Day

# Installation manual

1. Connect the 2 UBS cables from sensor black board to the PC where the game will run. (White USB: gestic sensor, Black USB: sliding bar)
2. Set the sliding bar device COM port to 'COM5'.
3. Compile console & ClientTCPTest projects.
4. Put compiled ClientTCPTest.exe inside console project's output folder.
5. Compile Unity project.
6. Run console.exe.
7. Run unity exe.
